module.exports = (function () {
  return {
    sql: {
      host: "localhost",
      port: "3306",
      user: "root",
      pass: "EnterPassword",
      db: "EnterDBName"
    },
    http: {
      http_port: 80,
      https_port: 443,
      whitelist: false,
      hostname: "loopback.rpgfarm.kr",
      pretty_html: true,
      server_ip: "127.0.0.1",
      port_autobind: false
    },
    recaptcha: {
      g_captcha_site_key: "Recaptcha Site Key",
      g_captcha_secret_key: "Recaptcha Secret Key"
    },
    mail: {
      mail_sv: "smtp.worksmobile.com",
      mail_id: "a-mail-sender@rpgfarm.com",
      mail_pw: "EnterPassword"
    },
    session: {
      secret: "EnterSecret"
    }
  }
})();
