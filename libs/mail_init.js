var nodemailer = require('nodemailer');
var ss = require('../config/server_settings');
module.exports = nodemailer.createTransport({
    host: ss.mail.server,
    auth: {
      user: ss.mail.id,
      pass: ss.mail.pass
    }
  });
