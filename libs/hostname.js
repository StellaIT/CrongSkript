var ss = require('../config/server_settings');
module.exports = function() {
    return function hostname( req, res, next ) {
        if (req.protocol == "http") {
          if (ss.http.http_port != 80 && ss.http.port_autobind == true) {
            req.hostname = req.hostname+':'+ss.http.http_port;
            res.locals.hostname = req.hostname+':'+ss.http.http_port;
          } else {
            req.hostname = req.hostname;
            res.locals.hostname = req.hostname;
          }
        } else {
          if (ss.http.https_port !== 443 && ss.http.port_autobind == true) {
            req.hostname = req.hostname+':'+ss.http.https_port;
            res.locals.hostname = req.hostname+':'+ss.http.https_port;
          } else {
            req.hostname = req.hostname;
            res.locals.hostname = req.hostname;
          }
        }
        next();
    };
};
