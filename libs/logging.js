module.exports = function(req, res, next) {
  if(req.user)
    console.log('[Access] '+req.user.id+'('+req.ip+'): '+req.path+' ('+req.method+')')
  else
    console.log('[Access] '+req.ip+': '+req.path+' ('+req.method+')')
  next();
};
