var ss = require('../config/server_settings');
var mysql = require('mysql');
var connection = mysql.createPool({
  host: ss.sql.host,
  port: ss.sql.port,
  user: ss.sql.user,
  password: ss.sql.pass,
  database: ss.sql.db,
  dateStrings: 'date'
});
module.exports = connection
